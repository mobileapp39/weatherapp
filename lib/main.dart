import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

void main() {
  runApp(WeatherApp());
}


class MyAppTheme extends StatelessWidget {
  static ThemeData appTheme() {
    return ThemeData(
      brightness: Brightness.dark,
      iconTheme: IconThemeData(
        color: Colors.white,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    throw UnimplementedError();
  }
}
  class WeatherApp extends StatefulWidget {
  @override
  State<WeatherApp> createState() => _WeatherAppState();
  }

  class _WeatherAppState extends State<WeatherApp> {
  @override
  Widget build(BuildContext context) {
  return MaterialApp(
  theme: MyAppTheme.appTheme(),
  debugShowCheckedModeBanner: false,
  home: Scaffold(
  appBar: PreferredSize(
  preferredSize: Size.square(58),
  child: AppBar(
  flexibleSpace: Container(
  decoration: BoxDecoration(
  image: DecorationImage(
  image: NetworkImage(
  "https://i.pinimg.com/564x/17/69/53/176953835a397dce72d41bf1730bd4c0.jpg"),
  fit: BoxFit.cover,
  ),
  )),
  ),
  ),
  body: Container(
  decoration: BoxDecoration(
  image: DecorationImage(
  image: NetworkImage(
  "https://i.pinimg.com/564x/17/69/53/176953835a397dce72d41bf1730bd4c0.jpg"),
  fit: BoxFit.cover,
  ),
  ),
  child: ListView(
  children: <Widget>[
  Weather(),
  AllDay(),
  Weekly(),
  ],
  ),
  ),
  ));
  }
  }

  Widget AllDay() {
  return Card(
  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
  color: Colors.grey.withOpacity(0),
  margin: EdgeInsets.all(10),
  child: SizedBox(
  height: 155,
  child: ListView(
  scrollDirection: Axis.horizontal,
  children: <Widget>[
    time_1(),
    time_2(),
    time_3(),
    time_4(),
    time_5(),
    time_6(),
    time_7(),
    time_8(),
    time_9(),
    time_10(),
    time_11(),
    time_12(),
  ],
  ),
  ),
  );
  }

  Widget Weather() {
  return Column(
  children: <Widget>[
    Text(
      "อ.เมืองชลบุรี",style: TextStyle(fontSize: 40, height: 2, color: Colors.white),
    ),
  Text(
  "27°",
  style: TextStyle(fontSize: 90, color: Colors.white),
  ),
  Text(
  "เมฆเป็นส่วนมาก",
  style: TextStyle(fontSize: 20, color: Colors.white70),
  ),
  Text(
  "สูงสุด:32°  ต่ำสุด:22°",
  style: TextStyle(fontSize: 20, height: 2, color: Colors.white70),
  )
  ],
  );
  }

  Widget time_1() {
  return Column(
  children: <Widget>[
  Padding(
  padding: EdgeInsets.only(
  top: 20,
  bottom: 5,
  left: 20,
  ),
  child: Text(
  "ตอนนี้",
  style: TextStyle(fontSize: 20, color: Colors.white),
  ),
  ),
  Padding(
  padding: EdgeInsets.only(
  top: 10,
  bottom: 5,
  left: 20,
  ),
  child: Icon(
  Icons.cloud,
  ),
  ),
  Padding(
  padding: EdgeInsets.only(
  top: 10,
  bottom: 10,
  left: 20,
  ),
  child: Text(
  "27°",
  style: TextStyle(fontSize: 20, color: Colors.white),
  ),
  ),
  ],

  );
  }

Widget time_2() {
  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(
          top: 25,
          bottom: 5,
          left: 20,
        ),
        child: Text(
          "21",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 5,
          left: 20,
        ),
        child: Icon(
          Icons.cloud,
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "26°",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    ],

  );
}

Widget time_3() {
  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(
          top: 25,
          bottom: 5,
          left: 20,
        ),
        child: Text(
          "22",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 5,
          left: 20,
        ),
        child: Icon(
          Icons.cloud,
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "25°",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    ],

  );
}

Widget time_4() {
  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(
          top: 25,
          bottom: 5,
          left: 20,
        ),
        child: Text(
          "23",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 5,
          left: 20,
        ),
        child: Icon(
          Icons.cloud,
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "25°",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    ],

  );
}

Widget time_5() {
  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(
          top: 25,
          bottom: 5,
          left: 20,
        ),
        child: Text(
          "00",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 5,
          left: 20,
        ),
        child: Icon(
          Icons.cloud,
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "24°",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    ],

  );
}

Widget time_6() {
  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(
          top: 25,
          bottom: 5,
          left: 20,
        ),
        child: Text(
          "01",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 5,
          left: 20,
        ),
        child: Icon(
          Icons.cloud,
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "24°",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    ],

  );
}

Widget time_7() {
  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(
          top: 25,
          bottom: 5,
          left: 20,
        ),
        child: Text(
          "02",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 5,
          left: 20,
        ),
        child: Icon(
          Icons.cloud,
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "24°",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    ],

  );
}

Widget time_8() {
  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(
          top: 25,
          bottom: 5,
          left: 20,
        ),
        child: Text(
          "03",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 5,
          left: 20,
        ),
        child: Icon(
          Icons.cloud,
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "23°",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    ],

  );
}

Widget time_9() {
  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(
          top: 25,
          bottom: 5,
          left: 20,
        ),
        child: Text(
          "04",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 5,
          left: 20,
        ),
        child: Icon(
          Icons.cloud,
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "23°",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    ],

  );
}

Widget time_10() {
  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(
          top: 25,
          bottom: 5,
          left: 20,
        ),
        child: Text(
          "05",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 5,
          left: 20,
        ),
        child: Icon(
          Icons.cloud,
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "22°",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    ],

  );
}

Widget time_11() {
  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(
          top: 25,
          bottom: 5,
          left: 20,
        ),
        child: Text(
          "06",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 5,
          left: 20,
        ),
        child: Icon(
          Icons.cloud,
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "22°",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    ],

  );
}

Widget time_12() {
  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(
          top: 25,
          bottom: 5,
          left: 20,
        ),
        child: Text(
          "06:40",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 5,
          left: 20,
        ),
        child: Icon(
          Icons.sunny_snowing,
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 20,
        ),
        child: Text(
          "อาทิตย์ขึ้น",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    ],

  );
}

  Widget textWeek() {
  return Row(
  children: <Widget>[
  Padding(
  padding: EdgeInsets.only(
  top: 20,
  bottom: 20,
  left: 10,
  ),
  child: Text(
  "พยากรณ์อากาศ 5 วัน",
  style: TextStyle(fontSize: 20,color: Colors.white54),
  ),
  ),
  ],
  );
  }

  Widget Day() {
  return Row(
  mainAxisAlignment: MainAxisAlignment.spaceBetween,
  children: <Widget>[
  Column(
  crossAxisAlignment: CrossAxisAlignment.start,
  children: <Widget>[
  Text(
  "วันนี้",
  style: TextStyle(fontSize: 20,color: Colors.white),
  ),
  Text(
  "พฤ.",
  style: TextStyle(fontSize: 20, height: 2.5,color: Colors.white),
  ),
  Text(
  "ศ.",
  style: TextStyle(fontSize: 20, height: 2.5,color: Colors.white),
  ),
  Text(
  "ส.",
  style: TextStyle(fontSize: 20, height: 2.5,color: Colors.white),
  ),
  Text(
  "อา.",
  style: TextStyle(fontSize: 20, height: 2.5,color: Colors.white),
  ),
  ],
  )
  ],
  );
  }

  Widget IconDay() {
  return Row(
  mainAxisAlignment: MainAxisAlignment.spaceBetween,
  children: <Widget>[
  Column(
  crossAxisAlignment: CrossAxisAlignment.start,
  children: <Widget>[
  Icon(
  Icons.cloud,
  size: 50,
  ),
  Icon(
    Icons.cloud,
    size: 50,
  ),
  Icon(
    Icons.cloud,
    size: 50,
  ),
  Icon(
    Icons.cloud,
    size: 50,
  ),
  Icon(
    Icons.cloudy_snowing,
    size: 50,
  ),
  ],
  )
  ],
  );
  }

  Widget temp() {
  return Row(
  mainAxisAlignment: MainAxisAlignment.spaceBetween,
  children: <Widget>[
  Column(
  crossAxisAlignment: CrossAxisAlignment.start,
  children: <Widget>[
  Text(
  "22° ===== 32°",
  style: TextStyle(fontSize: 20,color: Colors.white),
  ),
  Text(
  "22° ===== 31°",
  style: TextStyle(fontSize: 20, height: 2.5,color: Colors.white),
  ),
  Text(
  "22° ===== 29°",
  style: TextStyle(fontSize: 20, height: 2.5,color: Colors.white),
  ),
  Text(
  "21° ===== 26°",
  style: TextStyle(fontSize: 20, height: 2.5,color: Colors.white),
  ),
  Text(
  "20° ===== 30°",
  style: TextStyle(fontSize: 20, height: 2.5,color: Colors.white),
  ),
  ],
  )
  ],
  );
  }

  Widget Weekly() {
  return Card(
  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
  color: Colors.white.withOpacity(0),
    margin: EdgeInsets.all(10),
  child: SizedBox(
  height: 300,

  child: ListView(
  scrollDirection: Axis.vertical,
  children: <Widget>[
  Column(
  children: <Widget>[
  textWeek(),
  Row(
  mainAxisAlignment: MainAxisAlignment.spaceAround,
  children: <Widget>[
  Day(),
  IconDay(),
  temp(),
  ],
  )
  ],
  )
  ],
  ),
  ),
  );
  }
